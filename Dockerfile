FROM node:14 as base

WORKDIR /app

COPY package*.json ./

RUN npm i

COPY . .

RUN npm run build

COPY . .

USER node

CMD ["node","dist/app.js"]

