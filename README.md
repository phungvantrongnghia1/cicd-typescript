# cicd-typescript

Build ci/cd typescript nodejs

## Getting started

### Link: https://viblo.asia/p/auto-deploy-voi-docker-va-gitlab-ci-E375z4eRZGW#_dieu-kien-can-1

## Step Deploy

1.  Create EC2 and create ssh key add publish key into `nano ~/.ssh/authorized_keys`
    1.1 Create folder and add docker-compose.yml

2.  Add ENV into gitlap
    PATH_TO_PROJECT: đường dẫn tới project ở trên server
    SSH_PRIVATE_KEY: Private Key mà ta vừa tạo ở trên.
    // Nếu branch là protechted thì để giá trị biến môi trường là protected. Nhưng sẽ bị lỗi, hãy bỏ protected và làm lại
    Note that base 64 encoding is necessary to use an SSH key with the "masked" and "protected" properties.
    SSH_SERVER_IP: địa chỉ IP của server
    SSH_USER: tên user dùng để truy cập server

    // Default by gitlap
    CI_REGISTRY_USER  
    CI_REGISTRY_PASSWORD
    CI_REGISTRY

### 
 - Add public key gitlap runner to authorized_keys in /.ssh/authorized_keys
 - Encode your private RSA key cat my_private_key | base64 -w0 -> Coppy output into variable gitlab
 - Macos: base64 < ~/.ssh/id_rsa

