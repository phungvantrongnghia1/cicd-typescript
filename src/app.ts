import express from "express";
const app = express();
const port = 1812;

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/hello", (req, res) => {
  res.send("Hello World!");
});

app.get("/info", (req, res) => {
  res.send("Finish deploy with cicd!");
});

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});
